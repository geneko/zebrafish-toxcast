# README #

This repository contains the scripts for parsing and data analysis of the Zebrafish-ToxCast dataset.

**parseZebrafish-ToxCast.py**: This file parses the published Zebrafish experimental data and the EPA ToxCast data so we can extract information for our computational studies.

*Environment*:
Anaconda 4.1.1 (Python 3), FuzzyWuzzy 0.11.0, python-Levenshtein 0.12.0

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact