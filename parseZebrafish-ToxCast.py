#%%
# We are interested in combining the Zebrafish experimental data with data
# provided by the US Environmmental Protection Agency (USEPA).
#
# The Zebrafish experimental data used chemicals from the USEPA ToxCast Phase I
# and II Screenings.
#
# The Zebrafish data contains the name of the chemicals and the experimental
# result (mortality concentration).
#
# The ToxCast data contains the SMILES representation of each chemical
# structure we need for our computational studies.

import multiprocessing as mp

# Fuzzy string matching using the Levenshtein Distance
# https://github.com/seatgeek/fuzzywuzzy
# The python-Levenshtein library is also required otherwise search is slow.
from fuzzywuzzy import process as fw
import pandas as pd

#%% Set filenames of datasets
# The Zebrafish data was obtained from the Supplementary Data:
# http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3871932/
# fileZebrafish refers to the spreadsheet containing the name of the chemical
# and the experimental mortality concentration.
#
# The ToxCast data was obtained from the ToxCast December 2013 Chemical List
# and Annotations:
# https://www3.epa.gov/research/COMPTOX/previously_published.html
# fileToxCast1 refers to the spreadsheet containing the following information:
#     CASRN, Name, ShortName, SMILES, MW, Formula, ToxCast Phase
# fileToxCast2 refers to the spreadsheet containing the following information:
#     CASRN, IUPACName
#
# All files were originally in Excel format. They were converted into CSV
# as Pandas reads CSV files much faster than Excel files.
#
# The output file fileMatch contains all of the data that can be perfectly
# matched between the Zebrafish and ToxCast data.
# The output file fileUnmatch includes additional data for manual verification:
#     ZebrafishName, name matching score.

# Input filenames
fileZebrafish = 'toxsci_13_0620_File010.csv'
fileToxCast1 = 'ToxCast_Generic_Chemicals_2013_12_10.csv'
fileToxCast2 = 'TOX21S_v4a_8599_11Dec2013.csv'

# Output filenames
fileMatch = 'Zebrafish-ToxCast_Match.csv'
fileUnmatch = 'Zebrafish-ToxCast_Unmatch.csv'

#%% multiprocessing function for using FuzzyWuzzy to do string matching
def mpStringMatch(*args):
    # Input: A tuple containing name and candidate names to search against.
    #
    # Output: A tuple containing the name, best name, and distance score.

    name, candidates = args[0]  # Get first input.
    # FuzzyWuzzy Usage: https://github.com/seatgeek/fuzzywuzzy
    matchedName, matchedScore = fw.extractOne(name, candidates)
    return (name, matchedName, matchedScore)

#%%
def main():
        # Import data
        dataZebrafish = pd.read_csv(fileZebrafish, header=0, encoding='latin1')
        dataToxCast1 = pd.read_csv(fileToxCast1, header=0)
        dataToxCast2 = pd.read_csv(fileToxCast2, header=0, encoding='latin1')

        # Get specific columns of interest
        dataZebrafish = dataZebrafish[['TX.id.names', 'MORT']]
        dataToxCast1 = dataToxCast1[['CASRN', 'Name', 'ShortName', 'SMILES',
                                     'MW', 'Formula', 'Phase_I', 'Phase_II']]
        dataToxCast2 = dataToxCast2[['TS_CASRN', 'STRUCTURE_IUPAC']]

        # Rename columns
        dataZebrafish.rename(columns = {'TX.id.names': 'Name'}, inplace=True)
        dataToxCast2.columns = ['CASRN', 'IUPACName']

        # Merge ToxCast data
        # The unique CASRN identifier in dataToxCast1 has a leading whitespace
        # that needs to be stripped so we can merge with dataToxCast2.
        dataToxCast1['CASRN'] = dataToxCast1['CASRN'].str.strip()
        dataToxCast = dataToxCast1.merge(dataToxCast2, left_on='CASRN',
                                         right_on='CASRN', how='left')

        # Reorder columns
        dataToxCast = dataToxCast[['CASRN', 'Name', 'ShortName', 'IUPACName',
                                  'SMILES', 'Formula', 'MW', 'Phase_I',
                                  'Phase_II']]

        # Merge Zebrafish data with ToxCast data
        # The provided Zebrafish data used a unique identifier that is not in
        # the EPA datasets. In addition, the name of the chemical compound
        # (Tx.id.names) has had commas stripped out of the name. We need to
        # strip the commas from the Name/ShortName/IUPACName and match against
        # the Zebrafish names. For the data that does not perfectly match, we
        # will use fuzzy string matching via FuzzyWuzzy to try to match the
        # remaining data.

        # We want to use CASRN as the unique identifier for merging purposes.
        # First, strip commas from ToxCast names, then match Zebrafish Name
        # against ToxCast Name, ShortName, and IUPACName.
        nameToxCast = dataToxCast[['CASRN', 'Name']]
        nameToxCast['Name'] = nameToxCast['Name'].str.replace(',', '')
        nameZebrafishMatchN = dataZebrafish.merge(nameToxCast, on='Name',
                                                  how='inner')

        nameToxCast = dataToxCast[['CASRN', 'ShortName']].rename(columns =
                        {'ShortName': 'Name'})
        nameToxCast['Name'] = nameToxCast['Name'].str.replace(',', '')
        nameZebrafishMatchSN = dataZebrafish.merge(nameToxCast, on='Name',
                                                   how='inner')

        nameToxCast = dataToxCast[['CASRN', 'IUPACName']].rename(columns =
                        {'IUPACName': 'Name'})
        nameToxCast['Name'] = nameToxCast['Name'].str.replace(',', '')
        nameZebrafishMatchIN = dataZebrafish.merge(nameToxCast, on='Name',
                                                   how='inner')

        # Merge matching dataframes and drop duplicates
        df = [nameZebrafishMatchN, nameZebrafishMatchSN, nameZebrafishMatchIN]
        zebrafishMatch = pd.concat(df, ignore_index=True).drop_duplicates()

        # Try to match remaining Zebrafish data with ToxCast using FuzzyWuzzy
        # Create a dataframe consisting of unmatched 'Name' and 'MORT'
        # SQL: LEFT JOIN on NAME where CASRN is NULL
        zebrafishUnmatch = dataZebrafish.merge(zebrafishMatch[['Name',
                                                               'CASRN']],
                                               on='Name', how='left')
        zebrafishUnmatch = zebrafishUnmatch[zebrafishUnmatch['CASRN'].isnull()]
        zebrafishUnmatch.drop('CASRN', axis=1, inplace=True)

        # Create a dictionary matching the Name/ShortName/IUPACName with the
        # CASRN.
        dictCASRN = {} # {Name/ShortName/IUPACName: CASRN}
        nameToxCast = dataToxCast[['CASRN', 'Name', 'ShortName', 'IUPACName']]
        nameToxCast['Name'] = nameToxCast['Name'].str.replace(',', '')
        nameToxCast['ShortName'] = nameToxCast['ShortName'].str.replace(
                                                                    ',', '')
        nameToxCast['IUPACName'] = nameToxCast['IUPACName'].str.replace(
                                                                    ',', '')
        for row in nameToxCast.itertuples(index=False):
            for i in range(len(row) - 1):
                dictCASRN[row[i + 1]] = row[0]

        # Use fuzzy string matching to match names and store results in
        # dfUnmatch
        nameToxCast = list(dictCASRN.keys())
        nameZebrafish = zebrafishUnmatch['Name'].values.tolist()
        mpArgs = list(zip(nameZebrafish, len(nameZebrafish)*[nameToxCast]))

        # Parallelize fuzzy string matching
        pool = mp.Pool()
        results = pool.map(mpStringMatch, mpArgs)
        pool.close()
        pool.join()

        # Process results
        dfUnmatch = pd.DataFrame()
        for result in results:
            zebrafishName, matchedName, matchedScore = result
            casRN = dictCASRN[matchedName]
            dfUnmatch = dfUnmatch.append({'Score': matchedScore,
                                          'ZebrafishName': zebrafishName,
                                          'CASRN': casRN}, ignore_index=True)

        # Merge Zebrafish with ToxCast data
        # For matched data, We don't need Zebrafish names anymore, so drop it.
        zebrafishMatch.drop('Name', axis=1, inplace=True)
        zebrafishMatch = zebrafishMatch.merge(dataToxCast, on='CASRN',
                                              how='left')
        # For unmatched data, keep Zebrafish names so we can manually compare
        # against ToxCast data in case of low name matching scores.
        # Merge experimental data.
        zebrafishUnmatch = dfUnmatch.merge(dataZebrafish,
                                           left_on='ZebrafishName',
                                           right_on='Name', how='left')
        # Drop extra identical Name column and merge with ToxCast.
        zebrafishUnmatch.drop('Name', axis=1, inplace=True)
        zebrafishUnmatch = zebrafishUnmatch.merge(dataToxCast, left_on='CASRN',
                                                  right_on='CASRN', how='left')

        # Export data to CSV files
        zebrafishUnmatch.sort_values('Score', inplace=True)
        zebrafishUnmatch.set_index('Score', inplace=True)
        zebrafishMatch.set_index('CASRN', inplace=True)

        zebrafishMatch.to_csv(fileMatch, na_rep='NA')
        zebrafishUnmatch.to_csv(fileUnmatch, na_rep='NA')

#%% Execute script with multiprocessing support
if __name__ == '__main__':
    # freeze_support() is needed to run multiprocessing under Windows.
    mp.freeze_support()
    main()